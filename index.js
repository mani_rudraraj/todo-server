const express= require('express')
const app =express();
const cors = require("cors");
const pool = require("./db");

//middleware
app.use(cors());
app.use(express.json());

//ROUTES

//create
app.post("/todos", async (req, res) => {
  try {
    const { description } = req.body;
    const newTodo = await pool.query(
      "INSERT INTO todo (description) VALUES($1) RETURNING *",
      [description]
    );
    return res.json(newTodo.rows[0]);
  } catch (err) {
    console.log("err", err);
  }
});

//select all
app.get("/todos", async (req, res) => {
  try {
    const allTodos = await pool.query("SELECT * from todo");
  } catch (err) {
    console.log("err", err);
  }
});

//select with ID
app.get("/todos/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const todos = await pool.query("SELECT * FROM todo WHERE todo_id = $1", [
      id,
    ]);
  } catch (err) {
    console.log("err", err);
  }
});

//update with ID
app.put("/todos/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const { description } = req.body;
    const todos = await pool.query(
      "UPDATE todo SET description = $1 where todo_id = $2",
      [description, id]
    );
  } catch (err) {
    console.log("err", err);
  }
});

//update with ID
app.delete("/todos/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const todos = await pool.query("DELETE FROM todo where todo_id = $1", [id]);
    res.json("Todo was deleted.");
  } catch (err) {
    console.log("err", err);
  }
});

app.listen(5000, () => {
  console.log("------server started on port 5000--------");
});